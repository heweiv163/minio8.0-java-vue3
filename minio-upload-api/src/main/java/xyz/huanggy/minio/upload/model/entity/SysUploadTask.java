package xyz.huanggy.minio.upload.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;


import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@TableName("sys_upload_task")
public class SysUploadTask implements Serializable {

    private Long id;
    //分片上传的uploadId
    private String uploadid;
    //文件唯一标识（md5）
    private String identifier;
    //文件名
    private String filename;
    //所属桶名
    private String bucketname;
    //文件的key
    private String objectkey;
    //文件大小（byte）
    private Long totalsize;
    //每个分片大小（byte）
    private Long chunksize;
    //分片数量
    private Integer chunknum;

}
